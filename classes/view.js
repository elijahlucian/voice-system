class View {
  constructor(schema) {
    Object.assign(this, schema)
  }

  update(state, data) {
    // reset view
    this.project = {}
    this.script = {}
    this.actor = {}
    this.lines = {}
    this.chat = [{ name: 'elijah', text: 'test chat' }]

    // update view
    this.project = data.projects[state.project]
      ? data.projects[state.project]
      : this.emptyProject()
    this.script = state.script
      ? data.scripts[state.script]
      : this.emptyScript(state.project)
    this.actor = data.actors[state.actor]
    this.chat = state.chat.slice(-5).reverse()

    this.lines = {
      number: state.line + 1,
      previous: this.script.parsed[state.line - 1],
      current: this.script.parsed[state.line],
      next: this.script.parsed[state.line + 1],
    }
    return true
  }

  emptyProject() {
    return {
      id: 'empty',
      name: 'none',
      author: 'none',
    }
  }

  emptyScript(project) {
    return {
      id: 'none',
      name: 'No Script Selected',
      file: 'none',
      character: 'Please Select a file!',
      voice: 'Tell the director to load a file.',
      project,
      parsed: ['Please Load Script'],
    }
  }
}

module.exports = { View }
