class State {
  constructor(schema) {
    Object.assign(this, schema)
  }

  update(key,value) {
    if(key === 'project') {
      this.script = 'none'
    }
    this[key] = value
    return true
  }

  merge(newState) {
    for(let key in newState) {
      this[key] = newState[key]
    }
    return true
  }

  resetLines() {
    this.line = 0
    this.take = 0
    return true
  }

  load(datapoint, id) {
    this[datapoint] = id
    switch (datapoint) {
      case 'script':
        this.line = 0
        this.take = 0
        break;
    
      default:
        return false
    }
    return true

  }
}

module.exports = { State }