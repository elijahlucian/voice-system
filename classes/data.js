const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')

class Data {
  constructor(initialData) {
    Object.assign(this, initialData)
    this.projects = {}
    this.scripts = {}
    this.actors = {}
    this.scriptsPath = './data/files'
    this.seedsPath = './data/seeds'
    this.models = ['projects', 'scripts', 'actors']
  }

  parseScript(file) {
    // this should really just take a filename
    if (!file) return ['please select a file']
    let scriptPath = path.join(this.scriptsPath, file)
    let scriptBuffer = fs.readFileSync(scriptPath)
    return scriptBuffer
      .toString()
      .trim()
      .replace(/\r/g, '')
      .split('\n')
  }

  parseAllScripts() {
    for (let script in this.scripts) {
      this.scripts[script].parsed = this.parseScript(this.scripts[script].file)
    }
  }

  addProject(record, options = {}) {
    let id = this.snakeCaseIfy(record.name)
    record.id = id
    this.projects[id] = record
    let ymlFilePath = path.join(this.seedsPath, 'projects', `${record.id}.yml`)
    if (options.seed) return true
    this.saveYml(ymlFilePath, record)
    return true
  }

  deleteProject(id) {
    // rewrite yaml with deleted flag
    delete this.projects[id]
  }

  addScript(record, options = {}) {
    if (record.deleted) return false
    let id
    if (!record.id) {
      id = this.snakeCaseIfy(record.name)
      record.id = id
    } else {
      id = record.id
    }
    this.scripts[id] = record
    if (typeof record.parsed === 'string') {
      record.parsed = record.parsed.split(',')
    }
    if (!this.scripts[id].parsed) {
      if (record.file) {
        let txtFilePath = path.join(this.txtFilePath, record.filename)
        fs.writeFileSync(txtFilePath, record.file)
        this.scripts[id].parsed = this.parseScript(record.file)
      }
    }
    let ymlFilePath = path.join(this.seedsPath, 'scripts', `${record.id}.yml`)
    if (options.seed) return true
    this.saveYml(ymlFilePath, record)
    return true
  }

  updateScript(record) {
    let seedFile = `${record.project}_${record.id}.yml`
    this.scripts[record.id] = record
    let ymlFilePath = path.join(this.seedsPath, 'scripts', seedFile)
    this.saveYml(ymlFilePath, record)

    return true
  }

  deleteScript(id) {
    // update deleted field on the record
    let record = this.scripts[id]
    record.deleted = true
    this.updateScript(record)
    // or delete
    // delete this.scripts[id]
    return true
  }

  addActor(record, options = {}) {
    if (record.deleted) return
    let id = this.snakeCaseIfy(record.name)
    record.id = id
    this.actors[id] = record
    let ymlFilePath = path.join(this.seedsPath, 'actors', `${record.id}.yml`)
    if (options.seed) return true
    this.saveYml(ymlFilePath, record)
    return true
  }

  deleteActor(id) {
    delete this.actors[id]
  }

  seedDatabase() {
    for (var model of this.models) {
      // get each folder of ymls
      let modelPath = path.join(this.seedsPath, model)
      let files = fs.readdirSync(modelPath)
      for (var yml of files) {
        let filePath = path.join(modelPath, yml)
        let f = fs.readFileSync(filePath)
        let record = yaml.safeLoad(f)
        switch (model) {
          case 'scripts':
            this.addScript(record, { seed: true })
            break
          case 'projects':
            this.addProject(record, { seed: true })
            break
          case 'actors':
            this.addActor(record, { seed: true })
            break

          default:
            break
        }
      }
    }
  }

  // private

  saveYml(name, record) {
    let yml = yaml.safeDump(record)
    this.persist(name, yml)
  }

  persist(name, buf) {
    console.log('Writing file', name)
    fs.writeFileSync(name, buf)
  }

  snakeCaseIfy(name) {
    return name
      .toLowerCase()
      .replace(/\(/g, '')
      .replace(/\)/g, '')
      .replace(/ -/g, '')
      .replace(/\s/g, '_')
  }
}

module.exports = { Data }
