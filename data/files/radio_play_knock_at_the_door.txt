VOICE: Lights Out, everybody!
TWELVE CHIMES - WIND UP ON NINTH - ALL OUT WITH:
GONG:
ELLA:(SLOWLY - CONVERSATIONALLY, AND YET WITH A TENSE UNDER NOTE)
Afraid to die? Who isn't? . . . When I was just a kid I used to wake up in the night and see the dark pressing all around me and I'd get so scared
(BUILDING UP IN SEMI-HYSTERIA) I'd think I was dead - buried - I'd try to scream - I couldn't - my voice - I'd know I was dead - I'd know it - I'd throw off the cover -
(ACTUALLY SCREAMS OUT) I'd cry out, "Mother! Mother!" . . .
(RECOVERING SELF) Why - why did I yell out like that? Why - why should I be scared now? I want to die - I've got to die! . . .
it's cold here in the basement. . .
(SLOWLY) I wonder. . . if the grave is cold . .
Why don't I get it over with! The rope's around my neck, -
one step off the ladder and I'll hang and die! And I've got to die - I've got to die! . .
MAN'S VOICE:(DEEP PORTENTOUSLY - BACK) The wages of sin are death. (FADE) The wages of sin are death.
ELLA:(TAKES UP THE WORDS AT THE SECOND "THE WAGES, ETC." IN ABOVE SAYING THEN IN UNISON WITH THE MAN'S FADING SPEECH) The wages of sin are death. . .
Funny how I seem to hear the words the way my father used to say them. . . The wages of sin. But was it sin? No, it was hate! I killed her because I hated her! . . .
I remember the first time we met. Jay had just married me, and we were walking along the street to his house.
TRANSITIONAL PAUSE - VERY SHORT
JAY:(FADEIN FAST) Oh, Ella, you'll like my ma; really you will.
ELLA:(FLATLY, BUT WITH LIGHTER VOICE THAN IN HER MONOLOGUE INTERLUDES) Will I?
JAY:Sure, she's swell! Best woman in the world!
ELLA:Is she?
JAY:(HASTILY) I mean - well, you're my wife now, Ella! That proves what I think of you, don't it?
ELLA:I guess it does.
JAY:You and ma'll get along fine. (CHUCKLES) Ha, will this be a surprise to her! Well, here's the house! What do you think of it?
ELLA:(NOT TOO IMPRESSED) It's - it's all right. . .
JAY:Well, in we go! Ha - will ma's eyes pop when she sees you!
KNOCKING ON DOOR - HARD
JAY:(STILL LAUGHING IN ANTICIPATION) Pop right out. I'll bet you! Come on, ma! Open up! Got a surprise for ya!
DOOR OPENING
MRS. KRUGER:(IN FAST) Jay! Where have you been, son? Where - (AS SHE SEES) Oh!
JAY:Surprised, eh, ma? Meet Ella!
MRS. KRUGER:(TRAGEDY IN VOICE) Jay! You've brought home. . . a woman!
VERY SHORT TRANSITIONAL PAUSE
ELLA:(MONOLOGUE TONE AGAIN) When she said it that way - right from that minute I knew I hated her! And that's the way it was from then on. . .
I wasn't Jay's wife to her. . . but a woman. . . a stranger in her house! . . . And it was her house - and everything in it. Nothing Jay's - everything hers!
JAY:(OFF SLIGHTLY) Ma, can I use your car to take Ella ridin'? . . . (IN A LITTLE CLOSER) Ma, mind if Ella plants some roses in your garden.
(BACK) Ma, is it all right if Ella uses some of your towels (FADE) to make covers for the. . .
ELLA:That's the way it was - hers - hers - everything hers! All I had was Jay . . . and he wasn't much. . . .
(BUILDING) You can't blame me! You hear me, you can't blame me for not standin' it! All my life I'd had nothin' and at last I was married and I still had nothin'!
Her house, her car, her money, her son - I couldn't stand it I tell you! . . (IN CLOSE) So I. . . killed her. . . .
MALE VOICE:(BACK AND FADING) The wages of sin are death. . .
ELLA:Why do I keep hearin' Pa's words in my head? I'm goin' to kill myself - I'm goin' to do it! . . .
It's just that I want to sit here on top of the ladder and think and talk about everything for a little while.
It's the last chance I'll get. . . So like I said. . . I killed her. Oh, not right away. I stood for it all for weeks and months. -
but all the time inside of me something was talkin'. . .
VOICE:(SOFTLY - BUILDING BEHIND ELLA'S SPEECH) Kill her! . . . Kill her! Everything'll be yours! Kill her! Everything'll be yours!
(CONTINUING, REACHING CLIMAX AND CUTTING OUT WHERE ELLA SCREAMS "YES!")
ELLA:At first soft-like - and then louder and louder until my head was filled with it! Louder and louder till I couldn't stand it no more!
(UP) Louder and louder until I said "Yes!" I'll do it! Yes! . . . .
(DOWN AGAIN - TENSELY) Yes. Everything'd be mine then. . . Everything. . . I always wanted it that way. It had to be that way. . .
Here - in this basement - that's where it happened. . . Jay went to work - I came down here. Oh, it wasn't very hard to do what I had to do.
I remember every minute of it so well. . . every minute. . . I called her. . . and she came down into the basement.
TRANSITIONAL PAUSE - VERY SHORT
MRS. KRUGER:(FADEIN FAST) You called me, Ella?
ELLA:Yes, I called you.
MRS. KRUGER:What's the matter?
ELLA:Somebody took the lid off the sewer down here.
MRS. KRUGER:Land sakes alive, now who could have done that? Heavy iron cover like that - Jay wouldn't of done it!
ELLA:Is it deep down there!
MRS. KRUGER:'Course its deep! Tain't a sewer anyway - covers an old well this building was built over.
ELLA:Oh!
MRS. KRUGER:What are you "ohing" about. I heard my son, Jay tellin' you the same thing weeks ago.
ELLA:Yes, he did tell me. Funny I forgot. Oh, look!
MRS. KRUGER:(IN SURPRISE) Wha -
ELLA:Down in the well! Look!
MRS. KRUGER:I - I don't see nothing. Wha - (SCREAM - FADING USE ECHO CHAMBER ON FADE TO PAINT PICTURE OF HER FALLING INTO WELL - SCREAMS OUT BACK WITH)
LOUD SPLASH, BACK, AS SHE HITS WATER - SPLASHING CONTINUES FAR BACK AS WOMAN STRUGGLES IN WATER
MRS. KRUGER:(FAR BACK - DROWNING) Help! I'm drowning! (ETC. AD LIB - HER CRIES GROW MORE WATERY AND INDISTINCT BEHIND)
ELLA:(IMPASSIONEDLY) It's your well, isn't it, ma, - ma? Your well! Everything's yours! So stay in it!
SOUND OF MOVING HEAVY LID BACK IN PLACE
ELLA:Stay in it, ma-ma! Stay in it!
HEAVY LID MOVES IN PLACE - DROWNING CRIES OUT KNIFE-CLEAR AS LID SETTLES INTO ITS SEAT.
ELLA:(WILDLY) Your well, ma-ma! But everything else is mine now! (FADE FAST) Everything! Mine! Mine!
TRANSITIONAL PAUSE - VERY SHORT.
ELLA:(MONOLOGUE TONE) And that's the way it was. . . Easy. . . Too easy . . . Jay came home. He said:
JAY:(IN FAST) Say, Ella, where's ma?
ELLA:I dunno. She wasn't here when I got back from the store. Must've gone out.
JAY:Oh! Well, she'll be back. (FADE) Leave it to ma. . .
TRANSITIONAL PAUSE - VERY SHORT
ELLA:But she didn't come back. . . How could she? Jay went to the police. They came around. They asked questions. They went away. And nothing happened. It was as easy as that. .
MALE VOICE:(IN CLOSE - QUICKLY WHISPERING) Wages of sin are death. . . Wages of sin are death.
ELLA:(MISERABLY) Why do I keep hearin' that in my head? Why? Why? I'm gonna die - the rope around my neck.
Just a little bit longer of talkin'. Just a little. . . She - she was gone - and I had her house. . . and her son - and then I didn't., want him.
Always talking about her - morning and night - mama this and mama that - mama always did this this way and that that way -
I tell you he made me just sick just to look at him! Mama mama - I wanted to forget, forget, forget! And how could I forget with his fat lips saying, "ma-ma - ma-ma - mama!"
. . . And then, one day, I got an idea. (IN CLOSE) There was more room under that iron lid in the basement, wasn't there? . . .
(QUICKLY) That was it! Yes, that was it! Got free of her - now I'd get free of him! Sell the house quick - get out - away - far away!
Yes, that was it! A free woman! With my looks and all that money - ha-ha - would I have a time! . . I planned everything.
Told the neighbors Jay was goin' on a trip. Yeah, a long trip. Give me time to sell everything. Oh, I planned good! I'd say Jay'd written me to sell the place and join him out of town.
Oh, I tell you it was perfect. He came home that night - the night I was gonna let him join his ma-ma.
JAY:(FADE IN FAST) Anybody home?
ELLA:I'm always home.
JAY:(CHUCKLES) You said that just like ma-ma used to.
ELLA:(FLATLY) Did I?
JAY:Say, that reminds me - I got a new detective agency working on the case. They think that maybe she lost her memory somehow - you know like you read about - and -
ELLA:(INTERRUPTING) All right! All right!
JAY:But, Ella -
ELLA:Sit down and eat your supper! Want it to get cold?
JAY:(QUERULOUSLY) Don't see why you don't want to talk about findin' ma-ma! After all -
ELLA:Sit down and eat your supper! You'll find your ma-ma.
JAY:You think so, Ella?
ELLA:Sure.
JAY:Say, that's good, hearin' you say that! (FADE) Findin' my ma, that's somethin' I sure'd like to do. . .
TRANSITIONAL PAUSE - VERY SHORT
ELLA:(MONOLOGUE TONE) He sat down to eat. I'd made him a good supper - why not - his last supper. . .
(TENSELY) And then - it happened. We were sitting there eatin' - when there was a knocking at the door -
TRANSITIONAL PAUSE - VERY SHORT
FADEIN SOUND OF SLOW KNOCKING, BACK
JAY:Now who can that be?
ELLA:How should I know. Get up and answer it.
JAY:Yeah.
SOUND OF CHAIR BEHIND PUSHED BACK.
JAY:(FADE) Peddler, I'll bet ya. . .
SOUND OF OPENING DOOR, BACK - WIND, FAR BACK, WITH OPENING DOOR.
JAY:(BACK) Nobody here!
ELLA:Huh?
JAY:(BACK) Nobody here!
ELLA:Then shut the door and come on back and finish your supper.
SOUND OF CLOSING DOOR - WIND OUT
JAY:(FADEIN) Funny! I heard knockin' plain, didn't you?
ELLA:Kids playin' jokes. Go on eat. (SLOWLY) I want you to help me fix somethin' in the basement.
JAY:Fix what?
ELLA:You'll see. . . Finish your eatin' first.
JAY:O.K.
KNOCKING AGAIN, BACK
ELLA:Those crazy kids.
JAY:I'll -
ELLA:No, sit where ya are. (FADE) I'll go and -
JAY:Ella, wait!
KNOCKING AGAIN, BACK
ELLA:Wait for what?
JAY:Listen! That knocking - how funny it sounds.
ELLA:THOSE kids!
JAY:But it's. . . from the basement door.
ELLA:(AFTER TENSE PAUSE - DOWN, TENSELY) No. . .
KNOCKING AGAIN, BACK.
JAY:Wull! I'd better go see who -
ELLA:No! Jay, don't open that door! (FADE) Don't I tell ya.
JAY:(THRU HER FADE) Don't see why not! Somebody knockin' - I gotta see -
FUMBLING WITH DOOR.
ELLA:(IN FULL) Jay, leave the door closed!
JAY:Why? What's the matter with you? Gotta open it - gotta see -
DOOR OPENING - SLOWLY, CREAKILY
ELLA:(AS DOOR OPENS) No, Jay. (GASPS)
JAY:(TENSE PAUSE - UNBELIEVINGLY) No!
ELLA:(IN TENSE TERROR) Oh, no!
JAY:(UNBELIEVINGLY AT FIRST THEN WITH GROWING JOY) Ma-ma! Ma-ma!
TRANSITIONAL PAUSE - SHORT
ELLA:(MONOLOGUE TONE) Yeah. . . it was her all right. . . There she was. . . eyes glarin' - dirty, grey old hair plastered wet around her face. . .
standin' there. . . I could see her with my own eyes. . . and yet she was dead I tell you. . . dead - dead - dead! . .
Jay didn't know that. . . no, he took that dead thing by the arm, and he led her into the room, and he sat her down in a chair.
TRANSITIONAL PAUSE - VERY SHORT
JAY:(FADEIN - CHUCKLING HAPPILY) . . . Oh, ma-ma, ma-ma! You did come back! I knew you would! We both knew it, didn't we, Ella? Now, tell us, mama, where've you been?
Why did you go? Yeah, and why come back this funny way - the back way up the basement steps? Why, mama?
MRS. KRUGER:(TRIES TO SPEAK CROAKINGLY WITH GREAT EFFORT) I - I - I --
JAY:Mama! You're sick! Oh, Ella, look! She's drippin' wet! And it ain't rained in days!
Ella, quick, take her upstairs and put her to bed! Yeah, and maybe you'd better sleep with her tonite.
Keep her warm! Ella, why are you lookin' so funny?
SOUND OF THUD OF BODY ON FLOOR.
JAY:(UP) Ella! Ya fainted! (FADE) Ella, why did ya faint Why? . . .
TRANSITIONAL PAUSE - SHORT.
ELLA:(MONOLOGUE TONE) Sure I fainted. . . Me sleep with that! Me keep that warm! . . (IN CLOSE, IN MINGLED AWE AND DISGUST)
Oh, no. . . . I didn't wake up until next mornin'. Jay was leavin' for work - he told me she was sleepin' in her room - to take good care of her when she woke up.
Then he went away - happy his mama was back. When he was gone, I sat down there in the kitchen and waited.
FADE IN TICKING OF CLOCK AND HOLD BEHIND.
ELLA:I waited a long time - listenin' - listenin'. . . The clock hands crawled slow around and around. . .
but there was only the sound of its tickin' - nothin' from upstairs. . . nothin' from her. . .
CLOCK CHIMING FIVE, BACK, BEHIND.
It got on to five - soon he'd be comin' home. I had to know - I tell you I had to! I went upstairs. . .
SOUND OF GOING UPSTAIRS SLOWLY, BACK
...to HER room... I opened the door.
DOOR OPENING SLOWLY, BACK
Nobody there! I tell you nobody there! The bed not even slept in! She'd never been there, never!
I'd dreamed it - Jay'd dreamed it! (JOYFULLY) She wasn't there! She hadn't come back...But I had to be sure! I went down into the basement.
I pried up that old iron lid.
SOUND OF IRON LID SLIDING BACK.
There was the open well. I lit a lantern and held it in the black hole. . . (HOARSELY WHISPERING)
I looked in. She was there. Floatin' in the water like a big, fat - (IN DISGUST) Ughh. . . .
But she was still there! That's all that mattered. . . I piled things over the lid to hold it down - an old trunk - boxes - heavy things - that's what I did. . .
Jay came home after a while. He said:
JAY:(OFF SLIGHTLY) How's ma-ma?
ELLA:I said (OFF SLIGHTLY) All right. . . (IN FULL) He said--
JAY:(OFF SLIGHTLY) Call her down to dinner, will ya?
ELLA:I said (OFF) "Yeah, sure!" (IN FULL - MONOLOGUE TONE) - (CHUCKLE) Yeah, that's what I said - I knew ma-ma was all right - where she was. . .
We sat down to dinner - he kept talking about her - how funny it was that she still was sleepin' - how funny it was the way she came back.
But all the time I was laughing inside myself, because I knew what had happened was just a strange dream we both had had, because she was dead - dead and floatin' . . .
We sat there eatin' - I felt good-- a bad dream ended for this night Jay would join his mama - swimmin'. . Yeah...And then, just like the nite before - it happened.
FADEIN MEASURED KNOCKING ON DOOR, BACK.
JAY:Say, now who in the world's that?
KNOCKING AGAIN.
And - and it's the basement door again! Now what in the -
TEARING DOOR OPEN
(UP) Ma-ma! You!
ELLA:(SCREAMS BACK, SCREAM FADING QUICKLY)
[TRANSITIONAL PAUSE]
ELLA:(MONOLOGUE TONE) Yeah, I screamed - what good was that - there she was again, the water drippin' down off her face and clothes and puddlin' on the floor -
and this time I knew it was no dream. . . She came into the room - she sat down on a chair - Jay talking, talking all the time -
not understandin' yet, thinkin' that she'd wandered out again. . . No, he didn't understand . . . he thought the wet on her face was from the rain that was falling outside. . .
He didn't see, the way I saw, that the flesh on her face was - . .
THUNDER, FAR BACK
It started to thunder - and Jay said . . .
JAY:(FADEIN FAST) Oh, mama, mama, you should never of sneaked out on a night like this! Now to bed with you - in the mornin' I'll call the doctor. . .
 Ella! You take mama upstairs to bed! And this time be sure you sleep with her!
THUNDER, BACK
You know how scared she is o' thunder. Go on now. (FADE) She's soaked to the skin.
TRANSITIONAL PAUSE - VERY SHORT
ELLA:I just sat there - I tell you I couldn't move. Thinking of going upstairs with her, bein' in the same room with her - it froze me to my chair. . . Then Jay said. . .
JAY:(FADEIN) No, come to think of it, I'll go fetch the doctor right now! Can't take anymore chances! Ella, you take mama right upstairs and put her to bed.
Lie down with her - keep her warm 'til I get back (FADE) with Doc Williams. . .
TRANSITIONAL PAUSE - VERY SHORT.
ELLA:Then he was gone - gone after the doctor. I sat there the flesh on me crawlin'. The doctor'd come and he'd see that she was dead, dead a long time.
He'd be sure to see it, the way I saw it - dead, and bloated from the water, and sittin' there starin' at me. . . How long we sat there lookin' at each other I don't know...
and then her lips moved and it was like a cold wind. . .
FADEIN SOUND OF WIND, NOT TOO STRONGLY
MRS. KRUGER:(HOARSELY - WITH EFFORT) My son - said - to put me to bed. . . .
WIND UP AND THEN FADING OUT
ELLA:That's what she said - "my son said to put me to bed. . ." . . . And then I understood the whole thing!
She'd come back from the dead to keep me from killing him! And - And she wanted me along upstairs to - to - no! I wouldn't do it!
(DOWN TENSELY) I sat there! Without moving! Her lips moved again.
WIND EFFECT AS BEFORE, FADING IN.
MRS. KRUGER:(THERE IS MALIGNANT ENJOYMENT IN HER VOICE) He said - to keep me warm. Come upstairs, Ella - to keep me warm. .
WIND UP SLIGHTLY, THEN FADEOUT
ELLA:Me - keepin' that dead thing warm! Her wet cold drippin' skin against -
(UP) no! I wouldn't do it! She thought she'd make me crazy that way. Hold me in her bony arms until the sense in me ran out the way the well water was runnin' out o' her!
Make me crazy - crazy - somethin' to be locked away like they locked my own mother away - crazy - crazy - crazy! (BREATHING HEAVILY)
No, that wouldn't happen to me! She wouldn't make me crazy, not me! I got to my feet! I turned quick before she could stop me! The basement door!
OPENING DOOR, BACK
I locked it behind me!
SOUND OF LOCKING DOOR, BACK
I ran don the basement steps!
SOUND OF HER RUNNING DOWN STEPS
Oh, I knew how to cheat her! The sure way! The only way! (BREATHING HEAVILY) That's why I'm down here now!
The rope around my neck. The other end tied to the cross beam. I'll jump off this ladder - the rope'll stretch -
I'll be dead - dead - and she'll never get me crazy!
THUNDER BACK
ELLA:Still thunderin' - if I make noise as I choke, nobody'll hear me. And I'll die, - and I'll never see her again! Mother died crazy, but I'll die sane
- you hear me? Sane - sane! Why don't I do it now? There isn't much time - Jay and the doctor'll be comin' back, I've got to be dead then -
good and dead - or they'll find out the truth about her . . . Why don't I do it now - step off the ladder - it'll be over quick - I know it will!
The rope'll tighten quick - I'll hardly know what happened! (VOICE DEADENS SUDDENLY) Why don't I do it? . . . 'Cause I'm afraid! Afraid of dyin'. Terribly afraid. . .
That's why I've been talkin' here in the dark - cause I'm afraid - afraid to die. There'll be just nothin' when I'm dead - and I'm afraid of that nothin'.
I've always been afraid of it - when I was a kid . . . and now . . . It'll be so dark. . . empty . . . things'll go on - the world -
and I'll have nothin' but the dark. . . (TENSELY) Why should I die? Yes, why should I? If I left this house, she wouldn't follow me, would she? That's all she wants -
the house, her Jay. Well, she can have 'em. I'll go! I'll go right away! Far away! They'll never find me! Never! . . . (STRAINING) The rope - so tight around my neck!
Got to get it off. Got to hurry and get away - before Jay and the doctor - Can't get the rope off -
SOUND OF SLOW MEASURED FOOTSTEPS COMING DOWN, BACK, CONTINUING.
ELLA:Who - who's there? Who's coming down the steps? Is that you, Jay?...
STEPS OUT.
(SHUDDERING BREATH) You! You followed me down? How could you - I locked the door! . . . But then I put that iron lid over you, too, didn't I. . .
 And it didn't do much good. . . Listen - I'll go away! You hear me - I'll go away - this'll be your house and everything in it! Hear me -
 all yours again! And Jay - I'll leave him, too! I won't take anything with me - I'll just go - right away - out into the rain! And if I do that -
 if I never come back - that'll make everything all right, won't it? . . . No - you don't have to answer. I know it's all right with you. (LAUGHS)
 You see, I was going to kill myself. Still standin' up here - rope around my neck. But I won't have to do that now - will I - as long as I'm going away. . .
  (AS SHE PULLS ON NOOSE) Get the rope off. I'll go. You'll see me go! . . . (STRAINING) Made the noose so tight I can't seem to . . .
  (IN SUDDEN FRIGHT) Mrs. Kruger! What are you going to do?
SCUFFING SOUNDS, BACK
ELLA:(IN GROWING HORROR) You're - you're not going to climb up the ladder?. . . No, no, stay off! Stay off I tell you! Don't come closer! I can't stand your coming closer! Stay away! The ladder!
SOUND OF ROCKING LADDER
The ladder'll fall! The rope - it's still around my neck! If I fall, I'll --
SOUND OF LADDER FALLING OVER
ELLA:(AS LADDER FALLS OVER, SHE CRIES OUT - THE NOOSE TIGHTENS ABRUPTLY AND HER CRY CUTS CHOKINGLY OFF - SHE GURGLES AND GASPS FOR A FEW SECONDS - THEN BREATHLESSLY SAYS:)
No! No, you won't make me die! I've got hold of the rope with my hands! I won't choke this way - you see, you thing, you- I won't choke! I'll live!
I'll hold on to the rope and live! I don't want to die! I can't die! It isn't right to die! They'll think I killed myself! That isn't right I tell you!
You can't kill yourself! That's a sin! I know that now - I know it! I'll live! My arms are strong - I can hold on to the rope above my ahead for a long time!
Jay'll be home right away - he'll get me down! (SLOWLY THE STRAIN OF HER EFFORT TO HOLD HERSELF UP BEGINS TO SHOW IN HER VOICE) I'll live, I will - and you'll still be dead - dead!
I'll - I'll tell them you fell in the well - they'll believe me- they'll believe me!...
Well, say something! Say something! I - I know you're standing down there watching me!...You - you're waiting - waiting for my arms to get tired...so I'll let go...
But, I won't let go - I won't - I won't!...Jay'll be home - he'll be home before - got to hold on - will hold on...(BEGINNING TO BREATHE HEAVIER AND HEAVIER WITH THE EXERTION)
Can't let go....Can't hang...Got to live...Got to live...My arms!...(IN GROWING PANIC) So tired...Go to hold on...Got to hold on or the noose - the noose...
(CATCHES BREATH) Cramp in arm! (UP MADLY) Jay! Jay, hurry! Cramp in my arm! I can't hold on any more! Jay, hurry! I wouldn't have killed you,
Jay! Jay! Jay, I can't hold on - I - (UP IN HORRIBLE LAST APPEAL) Jay- y - y - y! (THE "JAY" BLENDS INTO HER SHRIEK AS SHE LETS GO OF ROPE)
SOUND OF ROPE SNAPPING TAUT
ELLA:(CHOKING SOUNDS - GROWING WEAKER AND WEAKER SLOWLY - THEN AT LAST, SILENCE)....
WE THEN HEAR A SOFT TRIUMPHANT CHUCKLE OF THE OLD WOMAN - THEN THE PAT-PAT OF AGED FOOTSTEPS ON THE CONCRETE FLOOR, THE SOUND OF IRON LID BEING MOVED BACK, THEN A LOUD SPLASH FROM DOWN IN THE WELL
GONG
ANNOUNCER:Lights Out, written especially for radio by Arch Oboler, comes to you each Wednesday from our Chicago studios.
