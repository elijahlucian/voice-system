TIFU by drinking an entire bottle of Louisiana Hot Sauce at a job interview in a failed attempt to be impressive.
I have had a number of job interviews recently that went poorly and did not result in securing employment.
I started to think I needed to do something during an interview to really stand out, be impressive, unique, and highly memorable. I thought it could be risky, but might work out.
I came up with the following idea: After concluding the interview, after the hand shakes, etc, when leaving the room stop, turn around, and say "There's one more thing you need to know about me."
Then pull out a bottle of hot sauce, down the entire bottle, slam it onto the ground and say "I can handle the heat." Nod confidently, leave the room.
Boom.
I imagined that they would be really impressed and wowed by such a performance.
Well it didn't pan out like I thought it would. It was only a small bottle of hot sauce, I figured it would be no big deal to actually do. I should have tested at home first, but I didn't.
I was nervous as a bitch-ass during the interview, but I was determined to follow through with the plan.
So I started exactly as described above. I was leaving, I turned around, maybe not with as much swagger as I'd imagined in my head, and I declared
"There's one more thing you need to know about me."
I pulled out the hot sauce bottle, almost dropped it, and started to open the bottle. In my head it was all one quick confident motion, like an electric Indiana Jones,
but instead I fumbled around and had a tough time getting it open. It felt like a nightmarish eternity but was probably only about 20 seconds.
Enough time for one of the interviewers to ask me what I was doing.
I didn't answer directly. Instead, after I got the bottle open, I repeated "There's one more thing you need to know about me." (But stuttering.)
Then I guzzled down the entire bottle of hot sauce. I instantly regretted it. My mouth and throat felt like lava was swirling around inside me.
I immediately started to gag and loudly cough, I was crying involuntarily. Tears hardcore streaming down my face. I was sweating like a terrible fool.
I desperately tried to scream "I can handle the heat" but just kept coughing before I could get anything out.
The interviewers were all standing up looking at me in horror and confusion.
A few seconds before I threw up all over the floor I knew it would happen, but I tried to hold it back. I couldn't.
I threw up all over the floor. It hurt as much on the way out as it did on the way in, if not more so.
The vomit felt like flaming barbed wire shredding its way through my neck.
I should mention a disturbing amount of fiery mucus was also leaking out of my nose uncontrollably.
After I finished throwing up I could not bear to look at the interviewers.
I hoarsely mumbled an apology and started to stumble as quickly as possible out the door.
I have never been more shamed in my life.
I didn't get the job.
