'use strict';

module.exports = {
  up: (q, Sequelize) => {
    return q.bulkInsert('Users', [{
      id: 'elijah_lucian',
      name: 'Elijah Lucian',
      social: '@eli7vh',
      email: 'elijahlucian@gmail.com',
      password: 'toffee15',
      tagline: "The diggity dankest",
      type: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Eric Svilpis',
social: '@cytokinesis',
tagline: 'Does he have free time?!',
id: 'eric_svilpis',
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      name: "Kaela Caron",
social: "@missyjopinup",
tagline: "She Preddy",
id: "kaela_caron",
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      name: "Nancy Situ",
social: "@exdevlin",
tagline: "Unassuming tiny rage",
id: "nancy_situ",
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      name: "Nat Black",
social: "@flippychips",
tagline: "Fellow of infinite jest",
id: "nat_black",
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),

    },{
      name: "Randolph West",
social: "@randolph",
tagline: "Officially disguised as a Canadian",
id: "randolph_west",
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      name: "Shylo Cliffe",
social: "@ltsnakeplissken",
tagline: "Definitely exists and is alive",
id: "shylo_cliffe",
password: 'toffee15',
type: 2,
      createdAt: new Date(),
      updatedAt: new Date(),

    }], {})
    
  },

  down: (q, Sequelize) => {
    return q.bulkDelete('Users', null, {});
  }
};
