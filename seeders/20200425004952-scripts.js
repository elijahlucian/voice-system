'use strict';

const  {orcs_apartments, orcs_elevator, orcs_engineer, orcs_mage, orcs_oger, orcs_spear_lady, vapsquad_mouth_warmups, voice_packs_action_pack, voice_packs_quake_arena_pack} = require('../data/seeds/scripts/scripts')

module.exports = {
  up: (q, Sequelize) => {
    return q.bulkInsert('Scripts', [{
      name: 'Orcs Apartments',
      file: 'orcs_apartments.txt',
      character: 'Many Different Voices',
      voice: 'Casual Real Life',
      project_id: 'orcs',
      id: 'orcs_apartments',
      parsed: JSON.stringify(orcs_apartments),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Orcs Elevator Guy',
      file: 'orcs_elevator.txt',
      character: 'Cutscene',
      voice: 'Crisp And Professional',
      project_id: 'david_zobrist_orcs',
      id: 'orcs_elevator',
      parsed: JSON.stringify(orcs_elevator),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Orcs Engineer',
      file: 'orcs_engineer.txt',
      character: 'Speaks With A Lisp',
      voice: 'Nerdy',
      project_id: 'orcs',
      id: 'orcs_engineer',
      parsed: JSON.stringify(orcs_engineer),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Orcs Mage',
      file: 'orcs_mage.txt',
      character: 'Spell Caster Mystical Orc',
      voice: 'Mystical, Wise',
      project_id: 'orcs',
      id: 'orcs_mage',
      parsed: JSON.stringify(orcs_mage),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Orcs Oger',
      file: 'orcs_oger.txt',
      character: 'Big Dumb One-Eyed',
      voice: 'Deep slow dumb voice',
      project_id: 'orcs',
      id: 'orcs_oger',
      parsed: JSON.stringify(orcs_oger),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Orcs Spear Lady',
      file: 'orcs_spear_lady.txt',
      character: 'Keto Vegan Valley Orc',
      voice: 'annoying',
      project_id: 'orcs',
      id: 'orcs_spear_lady',
      parsed: JSON.stringify(orcs_spear_lady),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Show Warmups',
      file: 'warmups.txt',
      character: 'WARM UP!',
      voice: 'YEAH!',
      project_id: 'vapsquad_show',
      id: 'show_warmups',
      parsed: JSON.stringify(vapsquad_mouth_warmups),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Action Voice Pack',
      file: 'voice_pack_action.txt',
      character: 'First Person Shooter Player',
      voice: 'think 80s action hero',
      project_id: 'voice_packs',
      id: 'action_voice_pack',
      parsed: JSON.stringify(voice_packs_action_pack),
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Quake Arena Style Voice Pack',
      file: 'voice_pack_quake.txt',
      character: 'First Person Shooter Player',
      voice: 'think 80s action hero',
      project_id: 'voice_packs',
      id: 'quake_arena_style_voice_pack',
      parsed: JSON.stringify(voice_packs_quake_arena_pack),
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    
  ], {});
    
  },

  down: (q, Sequelize) => {
    return q.bulkDelete('Scripts', null, {});
    
  }
};
