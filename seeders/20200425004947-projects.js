'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Projects', [{
      id: 'vapsquad_show',
      name: 'Vapsquad Show',
      user_id: 'elijah_lucian',
      website: 'vapsquad.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: 'voice_packs',
      name: 'Voice Packs',
      user_id: 'elijah_lucian',
      website: 'vapsquad.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: 'orcs',
      name: 'Orcs',
      user_id: 'david_zobrist',
      website: 'orcs',
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Projects', null, {});
  }
};

