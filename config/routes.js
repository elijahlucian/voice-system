const path = require('path')
const fs = require('fs')
const moment = require('moment')

const routes = server => {
  let { app } = server
  app.get('/', function(req, res) {
    let url = path.join(__dirname, 'build', 'index.html')
    console.log('GETTING ROOT > ', url)
    res.sendFile(url)
  })

  app.get('/api/state', (req, res) => {
    res.send({
      state: server.state,
      view: server.view,
    })
  })

  app.get('/api/data', (req, res) => {
    res.send({ data: server.data })
  })

  app.post('/api/state/recorder/:command', ({ params: { command } }, res) => {
    if (['trash', 'soundboard', 'restart'].includes(command)) {
      if (!server.state.recording) {
        res.send({
          error: `Recording is not enabled!`,
        })
        return
      }
    }
    switch (command) {
      case 'toggle_record':
        const requested_state = !server.state.recording
        if (requested_state) {
          if (!server.recordReady()) {
            res.send({ error: `You Must Set Up A Project and Script` })
            return
          }
          server.updateState({ recording: true })
          server.socketToEm('recorder/start_recording')
        } else {
          // stopping recording - discarding take
          server.updateState({ recording: false })
          server.socketToEm('recorder/stop_recording')
        }

        break
      case 'start_recording':
        if (!server.recordReady()) {
          res.send({ error: `You Must Set Up A Project and Script` })
          return
        }
        server.updateState({ recording: true })
        server.socketToEm('recorder/start_recording')
        break
      case 'trash':
        server.socketToEm('recorder/trash')
        break
      case 'soundboard':
        server.socketToEm('recorder/soundboard')
        break
      default:
        res.send({
          error: `Unknown Recorder Command => ${command}`,
        })
        return
    }
    res.send({ message: `received ${command}` })
  })

  app.post('/api/state/line/:command', ({ params: { command } }, res) => {
    switch (command) {
      case 'next':
        if (server.state.line >= server.view.script.parsed.length - 1) {
          res.send({ error: 'cannot go forward' })
          return
        }
        if (server.state.recording) server.socketToEm('recorder/next_line')
        server.updateState({
          line: server.state.line + 1,
          take: 0,
        })
        break
      case 'previous':
        if (server.state.line <= 0) {
          res.send({ error: 'cannot go back' })
          return
        }
        if (server.state.recording) server.socketToEm('recorder/previous_line')
        server.updateState({
          line: server.state.line - 1,
          take: 0,
        })

        break
      case 'reset':
        server.updateState({
          line: 0,
          take: 0,
        })
        break
      case 'redo':
        server.updateState({
          take: server.state.take + 1,
        })
        if (server.state.recording) server.socketToEm('recorder/new_take')
        break
      case 'goto':
        //get linenumber from body
        break
      default:
        res.send({ error: `Unknown Line Command => ${command}` })
        return false
    }

    // TODO: Save file on UI side
    res.send({ message: 'Command Accepted' })
  })

  app.post('/api/recordings', server.upload.single('wav'), (req, res) => {
    let { actor, project, script, line, take } = req.body
    console.log('ASDF')
    let recording = { actor, project, script, line, take }
    let ts = moment().unix()
    let filename = `${[actor, project, script, line, take, ts].join('_')}.wav`
    let filePath = path.join('data', 'recordings', filename)
    console.log('WRITING RECORDING', filePath)
    fs.writeFileSync(filePath, req.file.buffer)
    // TODO: Write recording record
    res.send({ message: 'Saved file!', recording })
  })

  app.post('/api/actors', ({ body: { name, social, tagline } }, res) => {
    let actor = { name, social, tagline }
    let created = server.data.addActor(actor)
    if (created) {
      res.send({ message: 'actor created', actor })
    } else {
      res.send({ error: 'the server could not create actor', actor })
    }
  })

  app.post('/api/projects', (req, res) => {
    let { name, author, website } = req.body
    let project = { name, author, website }
    let created = server.data.addProject(project)
    if (created) {
      res.send({ message: 'project created', project })
    } else {
      res.send({ error: 'the server could not create project', project })
    }
  })

  app.post('/api/scripts', server.upload.single('file'), (req, res) => {
    const { file, body } = req
    const { id, project, name, character, voice, parsed } = body
    let record
    if (file) {
      record = {
        id,
        project,
        name,
        character,
        voice,
        file: file && file.filename,
      }
    } else {
      record = { id, project, name, character, voice, parsed }
    }

    server.data.addScript(record)
    res.send({ message: 'record created!', record: record })
    server.updateView()
    server.socketToEm('data', server.data)
  })

  app.delete('/api/scripts/:id', (req, res) => {
    const { id } = req.params
    const deleted = server.data.deleteScript(id)
    console.log('deleted?', deleted)
    res.send({ message: 'record deleted!', id: id })
    server.updateView()
    server.socketToEm('data', server.data)
  })

  app.put('/api/scripts', (req, res) => {
    const { body } = req
    const { id, project, name, character, voice, parsed } = body
    console.log('updating script', id, name)
    server.data.updateScript({ id, project, name, character, voice, parsed })
    res.send({ message: 'record updated!', record: body.id })
    server.updateView()
    server.socketToEm('data', server.data)
  })

  app.post('/api/state/:key/:value', (req, res) => {
    const { key, value } = req.params
    const pluralized = {
      project: 'projects',
      script: 'scripts',
      actor: 'actors',
    }
    const pluralizedKey = pluralized[key]
    if (server.state.recording) server.socketToEm('recorder/stop_recording')
    const updated = server.updateState({
      [key]: value,
      line: 0,
      take: 0,
      recording: false,
    })
    if (key === 'project') server.updateState({ script: undefined })
    if (updated) {
      console.log('REQ', key, value)
      console.log(`${key} => ${server.data[pluralizedKey][value].name}`)
      res.send({ message: `changed ${key} to ${value}` })
    } else {
      res.send({ error: `could not change ${key} to ${value}` })
    }
  })

  return app
}

module.exports = { routes }
