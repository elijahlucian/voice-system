const sockets = server => {
  let { io } = server
  io.on('connection', socket => {
    server.socketCount++

    console.log(
      `a user (count ${server.socketCount}) connected - ID: ${socket.id}`
    )

    socket.on('checkpw', body => {
      if (body.password === 'dank') {
        socket.emit('unlock', body.id)
      }
    })

    socket.on('disconnect', () => {
      server.socketCount--
      // console.log('user has disconnected')
    })
  })
  return io
}

module.exports = { sockets }
