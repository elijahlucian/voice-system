const tmiOptions = {
  identity: {
    username: 'eli7vh',
    password: 'oauth:ofbqsg2ri38g9wl6xaig05v92ia0tz',
  },
  channels: [
    'vapsquad',
    // 'eli7vh'
  ],
}

const twitchEvents = server => {
  const rooms = {
    '87208062': 'eli7vh',
  }

  const { twitch } = server

  twitch.on('ping', (target, context, msg, self) => {
    // console.log('TWITCH PING')
  })

  twitch.on('message', (target, context, msg, self) => {
    let { username } = context
    server.chatNumber++

    console.log(
      `TWITCH CHAT (${rooms[context['room-id']]}) FROM ${
        context.username
      } > ${msg}`
    )
    if (!context['room-id']) return
    server.updateState({
      chat: [
        ...server.state.chat,
        { username, text: msg, id: server.chatNumber },
      ],
    })
  })
  return twitch
}

module.exports = { tmiOptions, twitchEvents }
