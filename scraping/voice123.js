const axios = require('axios')
const fs = require('fs')
const { range } = require('../utils/general')

range(1000).forEach(n => {
  if(n < 1) { return }

  axios.get(`https://voice123.com/api/providers/search?service=voice_over&page=${n}`)
  .then(response => {
    let body = response.data.providers.map(item => item.description)
    fs.writeFile(`./src/scraping/voice123-voiceTypes-${n}.js`, body, (err) => {console.log(err)})
    fs.writeFile(`./src/scraping/voice123-actors-${n}.js`, response.data.providers, 'utf-8', (err) => {console.log(err)})
  })
  .catch(err => console.log(err))
})

