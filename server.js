const express = require('express')
const socket = require('socket.io')
const bodyParser = require('body-parser')
const multer = require('multer')
const tmi = require('tmi.js')
const { tmiOptions, twitchEvents } = require('./config/twitch')

const { routes } = require('./config/routes')
const { sockets } = require('./config/sockets')

var stateSchema = require('./data/schema/state')
var viewSchema = require('./data/schema/view')

const { State } = require('./classes/state')
const { View } = require('./classes/view')
const { Data } = require('./classes/data')

class Server {
  constructor(params) {
    this.port = params.port || 3000
    this.app = express()
    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.app.use(bodyParser.json())
    this.app.use(express.static(__dirname + '/public'))
    this.storage = multer.diskStorage({
      destination: './data/files/',
      filename: (req, file, cb) => cb(null, file.originalname),
    })
    this.upload = multer({ storage: this.storage })
    this.http = require('http').Server(this.app)
    this.io = socket(this.http)
    this.twitch = new tmi.client(params.twitchAuth)
    this.state = new State(stateSchema)
    this.view = new View(viewSchema)
    this.data = new Data(params.data)
    this.socketCount = 0
    this.chatNumber = 0
  }

  init(initialState) {
    this.io = sockets(this)
    this.routes = routes(this)
    this.twitch = twitchEvents(this)
    this.twitch.connect()

    this.data.parseAllScripts()
    this.data.seedDatabase()
    this.state.merge(initialState)
    this.view.update(this.state, this.data)
  }

  start() {
    this.http.listen(this.port, () => {
      console.log('APP IS RUNNING =>', this.port)
    })
  }

  updateState(newState) {
    // anytime the state is updated, we must update the view
    if (this.state.merge(newState) && this.updateView()) return true
  }

  updateView() {
    // anytime the view is updated, we must update all the clients
    this.view.update(this.state, this.data)
    if (this.socketToEm('state', { state: this.state, view: this.view }))
      return true
  }

  socketToEm(room, body = {}) {
    this.io.emit(room, body)
    return true
  }

  recordReady() {
    return this.state.actor && this.state.script && this.state.project
  }
}

const server = new Server({
  port: process.env.PORT || 8080,
  twitchAuth: tmiOptions,
})

server.init({
  project: 'vapsquad_show',
  script: undefined,
  actor: 'shylo_cliffe',
  recording: false,
})

server.start()
