import React from 'react'
import ReactDOM from 'react-dom'
import { Route, BrowserRouter as Router } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import './creator-dashboard.css'

import Menu from './components/Menu'

import Overlay from './components/Overlay'

import Prompter from './components/Prompter'
import BoothUI from './components/BoothUI'
import Admin from './components/Admin'
import Recorder from './components/Recorder'

import Project from './components/Project'
import Script from './components/Script'
import Actor from './components/Actor'

import BridgeTroll from './components/BridgeTroll'

import * as serviceWorker from './serviceWorker'

const endpoint = window.location.href.split('/')[3]
const endpoint_check = ['prompter', 'overlay'].includes(endpoint)
console.log('endpoint', endpoint_check)
const routing =
  endpoint_check || localStorage.password ? (
    <Router>
      <div>
        <Route exact path="/" component={Menu} />
        <Route path="/menu" component={Menu} />
        <Route path="/prompter" component={Prompter} />
        <Route path="/overlay" component={Overlay} />
        <Route path="/booth" component={BoothUI} />
        <Route path="/recorder" component={Recorder} />
        <Route path="/admin" component={Admin} />
        <Route path="/project" component={Project} />
        <Route path="/script" component={Script} />
        <Route path="/actor" component={Actor} />
      </div>
    </Router>
  ) : (
    false
  )

ReactDOM.render(routing || <BridgeTroll />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
