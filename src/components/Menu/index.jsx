import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'

class Menu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    axios.get('/api/state').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('state', data => this.setState(data))
    socket.connect()
  }

  render() {
    console.log(this.state)
    if (this.state.view) {
    }

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="menu-heading-1">MAIN MENU</h1>
          <h3 className="menu-heading-3">Stream</h3>
          <a href="/overlay">Overlay</a>
          <h3 className="menu-heading-3">Admin</h3>
          <a href="/prompter">Prompter</a>
          <a href="/admin">Admin Panel</a>
          <a href="/booth">Controller</a>
          <a href="/recorder">Recorder</a>
          <h3 className="menu-heading-3">Creator Dash</h3>
          <a href="/project">Projects</a>
          <a href="/script">Scripts</a>
          <a href="/actor">Actor</a>
          <br></br>
          <h1>Todo</h1>
          <div className="todo">
            <p> - HTTPS</p>
            <p> - add actor dialog ?</p>
            <p> - reset project form on submit</p>
            <p> - add copy paste in box parsing</p>
            <p> - add actors to overlay</p>
            <p> - Get Overlay thing from Kaela</p>
            <p> - </p>
          </div>
        </header>
      </div>
    )
  }
}

export default Menu
