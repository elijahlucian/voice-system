import React from 'react'
import io from 'socket.io-client'

export default class BridgeTroll extends React.Component {
  state = {}

  componentDidMount() {
    this.socket = io()
    this.socket.on('unlock', body => {
      if (body === this.socket.id) {
        localStorage.password = 'true'
        window.location.reload()
      }
    })
  }

  checkpw = ({ target: { value } }) => {
    this.socket.emit('checkpw', { password: value, id: this.socket.id })
  }

  render() {
    return (
      <div className="App-header">
        <h1>Enter the Password!</h1>
        <input onChange={this.checkpw}></input>
      </div>
    )
  }
}
