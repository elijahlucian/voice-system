import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'

class Prompter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    axios.get('/api/state').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('state', data => {
      this.setState(data)
    })
  }

  render() {
    if (!this.state.view) {
      return <div className="overlay">LOADING!</div>
    }

    const { actor, project, script } = this.state.view

    const take = this.state.state.take + 1
    const lineNumber = this.state.state.line + 1
    const lineNumberMax = this.state.view.script.parsed.length
    const fontSize = 40 - 20 * (this.state.view.lines.current.length / 200)
    const line = {
      current: {
        text: this.state.view.lines.current,
        size: fontSize,
      },
    }

    return (
      <div className="App">
        <header className="overlay">
          <div className="overlay-part">
            <p className="overlay-part-info">Actor - {actor.ame}</p>
            <p className="overlay-part-info">Project - {project.ame}</p>
            <p className="overlay-part-info">Script - {script.name}</p>
            <p className="overlay-part-info">Take# - {take}</p>
            <p className="overlay-part-info">
              Line# - {lineNumber} / {lineNumberMax}
            </p>
          </div>
          {line && (
            <>
              <div
                className="overlay-current"
                style={{ fontSize: line.current.size }}
              >
                {line.current.text}{' '}
              </div>
            </>
          )}
        </header>
      </div>
    )
  }
}

export default Prompter
