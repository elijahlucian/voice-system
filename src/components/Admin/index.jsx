import React from 'react'
import './index.css'
import axios from 'axios'
import lodash from 'lodash'
import io from 'socket.io-client'
import { Link } from 'react-router-dom'

import { Button } from 'react-bootstrap'

class Admin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    axios.get('/api/state').then(res => {
      this.setState(res.data)
    })
    axios.get('/api/data').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('state', data => this.setState(data))
    socket.on('data', data => this.setState(data))
    // socket.connect()
  }

  initVM = vm => {
    vm.projects = []
  }

  pickData = (data, value) => {
    axios.post(`/api/state/${data}/${value}`).then(res => {
      let { error, message } = res.data
      this.setState({ error, message })
    })
  }

  navTo = ({ target: { value } }) => {
    console.log(value)
  }

  render() {
    console.log(this.state)

    let vm = {}
    if (this.state.data && this.state.state) {
      this.initVM(vm)
      vm.projects = []
      vm.scripts = []
      vm.actors = []
      for (let id in this.state.data.projects) {
        let project = this.state.data.projects[id]
        vm.projects.push(
          <button
            className="admin-button"
            onClick={() => this.pickData('project', id)}
            id={id}
            key={id}
          >
            {project.name}
          </button>
        )
      }

      let availableScripts = lodash.filter(this.state.data.scripts, {
        project: this.state.state.project,
      })
      for (let script of availableScripts) {
        vm.scripts.push(
          <button
            className="admin-button"
            onClick={() => this.pickData('script', script.id)}
            key={script.id}
          >
            {script.name}
          </button>
        )
      }

      for (let id in this.state.data.actors) {
        let actor = this.state.data.actors[id]
        vm.actors.push(
          <button
            className="admin-button"
            onClick={() => this.pickData('actor', id)}
            key={actor.id}
          >
            {actor.name}
          </button>
        )
      }
    }

    return (
      <div className="admin-panel">
        <div className="messages">
          <h1>ADMIN PANEL</h1>
          {this.state.error && <div className="error">{this.state.error}</div>}
          {this.state.message && (
            <div className="message">{this.state.message}</div>
          )}
        </div>
        <div className="admin">
          <div className="admin-item">
            <div className="admin-header">
              <h3>Choose Project</h3>
              <Link to="project">New Project</Link>
            </div>
            <div className="admin-buttons">{vm.projects}</div>
          </div>
          <div className="admin-item">
            <div className="admin-header">
              <h3>Choose Script</h3>
              <Link to="script">New Script</Link>
            </div>
            <div className="admin-buttons">{vm.scripts}</div>
          </div>
          <div className="admin-item">
            <div className="admin-header">
              <h3>Choose Actor</h3>
              <Link to="actor">New Actor</Link>
            </div>
            <div className="admin-buttons">{vm.actors}</div>
          </div>
        </div>
      </div>
    )
  }
}

export default Admin
