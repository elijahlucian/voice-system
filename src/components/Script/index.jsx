import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'
import { Form, Button } from 'react-bootstrap'

class ScriptCreator extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {
        name: '',
        character: '',
        voice: '',
        filename: '',
        project: -1,
      },
    }
  }

  submitDisabled() {
    let v = !(
      this.state.form.name &&
      this.state.form.character &&
      this.state.form.voice &&
      this.state.form.project !== -1 &&
      (this.state.form.file || this.state.form.parsed)
    )
    return v
  }

  resetState = () => {
    let form = {
      name: '',
      character: '',
      voice: '',
      filename: '',
      project: -1,
    }
    this.setState({ form })
  }

  componentDidMount() {
    axios.get('/api/data').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('data', data => this.setState(data))
    // TODO: compare the files and update fields, or show conflicts
  }

  pasteText = e => {
    const { form } = this.state
    const parsed = e.target.value
      // .replace(/\. /g, '.\n')
      // .replace(/! /g, '!\n')
      // .replace(/\? /g, '?\n')
      .split('\n')
      .filter(item => {
        return item.length > 0
      })
    form.parsed = parsed
    this.setState({ form })
  }

  handleKeyPress = event => {
    let key = event.key
    if (key === 'Enter') {
      event.preventDefault()
      this.addLine()
    }
  }

  handleSubmit = event => {
    event.preventDefault()
    if (this.submitDisabled()) {
      this.setState({ error: 'you must fill in all fields' })
      return
    }
    let { form, file } = this.state

    if (this.state.form.id) {
      axios.put('/api/scripts', form).then(({ data: { error, message } }) => {
        this.setState({ error, message })
      })
    } else {
      const data = new FormData()
      form.id = form.name.toLowerCase().replace(/\s/g, '_')
      for (let key in form) {
        data.append(key, form[key])
      }
      data.append('file', file)
      data.append('filename', `${form.id}.txt`)
      axios.post('/api/scripts', data).then(({ data: { error, message } }) => {
        this.setState({ error, message })
      })
    }
  }

  selectScript = e => {
    let id = e.target.value
    let script = this.state.data.scripts[id]
    let { name, character, voice, project, parsed, file } = script
    let form = { name, character, voice, project, id, parsed, file }
    this.setState({ form })
  }

  deleteScript = e => {
    axios.delete(`/api/scripts/${this.state.form.id}`, res => {})
    this.resetState()
  }

  selectProject = e => {
    let { form } = this.state
    form.project = e.target.value
    this.setState({ form })
    console.log(form)
  }

  useEditor = e => {
    let { form } = this.state
    form.parsed = []
    this.setState({ form })
  }

  editLine = e => {
    let { form } = this.state
    let { id, value } = e.currentTarget
    form.parsed[id] = value
    this.setState({ form })
  }

  addLine = e => {
    let { form } = this.state
    form.parsed.push('')
    this.setState({ form })
  }

  removeLine = e => {
    let { form } = this.state
    let { value } = e.currentTarget
    form.parsed.splice(value, 1)
    this.setState({ form })
  }

  handleChange = e => {
    let { form } = this.state
    let { id, value } = e.currentTarget
    form[id] = value
    this.setState({ form })
  }

  handleFile = e => {
    let file = this.fileUpload.files[0]
    this.setState({ file })
  }

  render() {
    console.log('S', this.state)
    let vm = {}
    let submitIsDisabled = true
    if (this.state.data) {
      submitIsDisabled = this.submitDisabled()

      let projects = this.state.data.projects
      let scripts = this.state.data.scripts
      vm.parsed = this.state.form.parsed ? (
        <>
          <h1>Edit Script</h1>
          {this.state.form.parsed.map((item, i) => {
            return (
              <Form.Group key={i} className="script-edit-line" controlId={i}>
                <Form.Control
                  onKeyDown={this.handleKeyPress}
                  as="textarea"
                  key={i}
                  autoFocus={
                    i === this.state.form.parsed.length - 1 ? true : false
                  }
                  rows={Math.ceil(this.state.form.parsed[i].length / 47)}
                  onChange={this.editLine}
                  value={this.state.form.parsed[i]}
                ></Form.Control>
                <button
                  type="button"
                  value={i}
                  onClick={this.removeLine}
                  tabindex="-1"
                >
                  X
                </button>
              </Form.Group>
            )
          })}
          <Form.Group>
            <button type="button" onClick={this.addLine}>
              + Add Line
            </button>
          </Form.Group>
        </>
      ) : (
        false
      )

      vm.projectOptions = Object.keys(projects).map((item, i) => {
        return (
          <option value={item} key={i}>
            {projects[item].name}
          </option>
        )
      })
      vm.scriptOptions = Object.keys(scripts).map((item, i) => {
        if (!scripts[item].deleted) {
          return (
            <option value={item} key={i}>
              {scripts[item].name}
            </option>
          )
        } else {
          return
        }
      })
    }

    return (
      <div className="App">
        <header className="creator">
          <h1>Script Creator Dashboard</h1>
          <Form
            onSubmit={this.handleSubmit}
            className="creator-form"
            encType="multipart/form-data"
          >
            <Form.Group controlId="project">
              <Form.Label>Load Script</Form.Label>
              <Form.Control
                as="select"
                onChange={this.selectScript}
                defaultValue={-1}
                value={this.state.form.scripts}
              >
                <option disabled value={-1}>
                  - Load A Script -
                </option>
                {vm.scriptOptions}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="project">
              <Form.Label>Select Project</Form.Label>
              <Form.Control
                as="select"
                onChange={this.selectProject}
                value={this.state.form.project}
              >
                <option disabled value={-1}>
                  - Choose a Project -
                </option>
                {vm.projectOptions}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="name">
              <Form.Label>Script Name</Form.Label>
              <Form.Control
                type="text"
                onChange={this.handleChange}
                value={this.state.form.name}
              ></Form.Control>
            </Form.Group>
            <Form.Text></Form.Text>
            <Form.Group controlId="character">
              <Form.Label>Character Description</Form.Label>
              <Form.Control
                type="text"
                onChange={this.handleChange}
                value={this.state.form.character}
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="voice">
              <Form.Label>Voice Type</Form.Label>
              <Form.Control
                type="text"
                onChange={this.handleChange}
                value={this.state.form.voice}
              ></Form.Control>
            </Form.Group>
            {vm.parsed || (
              <Form.Group controlId="filename">
                <Form.Label>Paste Text or Open Editor</Form.Label>
                <div className="scripts-end-of-form-buttons">
                  <Form.Control
                    as="textarea"
                    onChange={this.pasteText}
                  ></Form.Control>
                  <Button type="button" onClick={this.useEditor}>
                    Open Editor
                  </Button>
                </div>
              </Form.Group>
            )}
            <div className="messages">
              {this.state.error && (
                <div className="error">{this.state.error}</div>
              )}
              {this.state.message && (
                <div className="message">{this.state.message}</div>
              )}
            </div>
            <div className="scripts-end-of-form-buttons">
              <Button
                disabled={submitIsDisabled}
                variant="primary"
                type="submit"
              >
                {this.state.form.id ? 'Update Script' : 'Create Script'}
              </Button>
              <Button type="button" onClick={this.resetState}>
                Reset Form
              </Button>
              {this.state.form.id && (
                <Button type="button" onClick={this.deleteScript}>
                  Delete Script
                </Button>
              )}
            </div>
          </Form>
        </header>
      </div>
    )
  }
}

export default ScriptCreator
