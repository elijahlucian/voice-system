import React from 'react'

export default props => {
  return (
    <audio
      className={`recorder-take ${props.delivered}`}
      controls
      src={URL.createObjectURL(props.audioSource)}
    ></audio>
  )
}
