import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'
import Recording from './Recording'
import moment from 'moment'
import p5 from 'p5'
import 'p5/lib/addons/p5.sound'

import { Button } from 'react-bootstrap'

// p5js is another option
// https://p5js.org/reference/#/p5.SoundRecorder

export default class Recorder extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      recordings: [],
      soundboard: [],
      trash: [],
      count: 0,
    }
    this.audioChunks = []
    this.mic = new p5.AudioIn()
    this.mic.start()
    this.soundRec = new p5.SoundRecorder()
    this.soundRec.setInput(this.mic)
    this.soundFile = new p5.SoundFile()
  }

  increment = () => {
    this.setState({ count: this.state.count + 1 })
  }

  startRecording = () => {
    // get sound file...
    this.setState({ message: 'Starting Recording...' })
    this.soundRec.record(this.soundFile)
  }

  handleRecordClick = e => {
    axios
      .post(`/api/state/recorder/${e.target.id}`)
      .then(({ data: { message, error } }) => {
        console.log(message, error)
        this.setState({ message, error, count: this.state.count + 1 })
      })
  }

  throwAwayRecordingAndStop = () => {
    this.soundRec.stop()
    this.soundFile = new p5.SoundFile()
    this.setState({ message: 'throwing away recording and stopping!' })
  }

  throwAwayRecording = () => {
    this.soundRec.stop()
    this.soundFile = new p5.SoundFile()
    this.soundRec.record(this.soundFile)
    // save recording to browser in trash folder.
    this.setState({ message: 'throwing away recording, continuing!' })
  }

  writeToSoundboard = () => {
    this.soundRec.stop()
    const blob = this.soundFile.getBlob()
    this.saveSoundboardTake(blob)
    this.soundFile = new p5.SoundFile()
    this.soundRec.record(this.soundFile)
    this.setState({ message: 'saving audio to soundboard!' })
  }

  saveTakeAndContinue = () => {
    this.soundRec.stop()
    const blob = this.soundFile.getBlob()
    this.saveScriptTake(blob)
    this.setState({ message: 'saving take to server!' })
    this.soundFile = new p5.SoundFile()
    this.soundRec.record(this.soundFile)
  }

  saveSoundboardTake = blob => {
    const {
      state: { actor },
    } = this.state
    const ts = moment().unix()
    const filename = `${actor}_${ts}.wav`

    const form = new FormData()

    form.append('wav', blob, filename)
    form.append('actor', actor)
    form.append('project', 'soundboard')
    form.append('script', 'soundboard')
    form.append('line', 0)
    form.append('take', 0)
    this.sendRecording(form)

    this.setState({
      soundboard: [...this.state.soundboard, { blob }],
    })
  }

  saveScriptTake = blob => {
    const {
      state: { project, actor, script, line, take },
    } = this.state

    const ts = moment().unix()
    const filename = `${[actor, project, script, line, take, ts].join('_')}.wav`

    const form = new FormData()

    form.append('wav', blob, filename)
    form.append('take', take)
    form.append('line', line)
    form.append('actor', actor)
    form.append('project', project)
    form.append('script', script)
    this.sendRecording(form)

    this.setState({
      recordings: [
        ...this.state.recordings,
        { blob, project, actor, script, line, take },
      ],
    })
  }

  checkRecording = e => {
    console.log(this.audioChunks)
  }

  sendRecording = form => {
    axios.post('/api/recordings', form).then(({ message, error }) => {
      this.setState({ message, error })
    })
  }

  componentDidMount() {
    axios.get('/api/state').then(({ data }) => {
      this.setState(data)
    })
    var socket = io()
    socket.on('recorder/trash', body => {
      this.throwAwayRecording()
      this.increment()
    })

    socket.on('recorder/new_take', body => {
      this.saveTakeAndContinue()
      this.increment()
    })

    socket.on('recorder/next_line', body => {
      this.saveTakeAndContinue()
      this.increment()
    })

    socket.on('recorder/previous_line', body => {
      this.throwAwayRecordingAndStop()
      this.increment()
    })
    socket.on('recorder/start_recording', body => {
      this.startRecording()
      this.increment()
    })
    socket.on('recorder/stop_recording', body => {
      this.throwAwayRecordingAndStop()
      this.increment()
    })

    socket.on('recorder/soundboard', body => {
      this.writeToSoundboard()
      this.increment()
    })

    socket.on('state', ({ state }) => {
      this.setState({ state })
    })
  }

  mapRecordingArray = recordings => {
    return recordings.map((recording, i) => {
      return (
        <div key={i} class="recording-take">
          <h3>
            {recording.actor}: {recording.script} => {recording.line}
          </h3>
          <Recording key={i} audioSource={recording.blob} />
        </div>
      )
    })
  }

  render() {
    if (!this.state.state && !this.state.view) {
      return <div>LOADING</div>
    }

    const recordings = this.mapRecordingArray(this.state.recordings)
    const soundboard = this.mapRecordingArray(this.state.soundboard)
    const { recording } = this.state.state

    console.log('STATE', this.state)
    console.log('RECORDINGS', recordings)

    return (
      <div className="App">
        <header className="recorder">
          {this.state.view && this.state.state && (
            <div className="part-info-header">
              <p className="part-info">{this.state.view.actor.name}</p>
              <p className="part-info">{this.state.view.project.name}</p>
              <p className="part-info">{this.state.view.script.name}</p>
              <p className="part-info">Take - {this.state.state.take}</p>
              <p className="part-info">
                {this.state.state.line} / {this.state.view.script.parsed.length}
              </p>
            </div>
          )}

          <div className="messages">
            {this.state.error && (
              <div key={this.state.count} className="system-message error">
                {this.state.error}
              </div>
            )}
            {this.state.message && (
              <div key={this.state.count} className="system-message message">
                {this.state.message}
              </div>
            )}
          </div>
          <h1>RECORDER</h1>
          <div className={`recorder-state ${recording && 'recording'}`}></div>
          <div className="recorder-controls">
            <Button
              id="start_recording"
              onClick={this.handleRecordClick}
              disabled={recording}
            >
              Start Auto Recording
            </Button>
            <Button
              id="stop_recording"
              onClick={this.handleRecordClick}
              disabled={!recording}
            >
              Stop Auto Recording
            </Button>
          </div>
          <div className="recordings">
            <h1>Recordings</h1>
            {recordings}
          </div>
          <div className="recordings">
            <h1>Soundboard</h1>
            {soundboard}
          </div>
        </header>
      </div>
    )
  }
}
