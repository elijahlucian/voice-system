import React from 'react'
import axios from 'axios'
import { Form, Button } from 'react-bootstrap'

export default class Actor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      actors: {},
      ids: [],
      form: {},
      fields: ['name', 'social', 'tagline'],
      newRecord: false,
    }
  }

  valid = () => {
    const { name, social, tagline } = this.state.form
    if (name && social && tagline) return true
  }

  unsaved = () => {
    const { name, social, tagline } = this.state.form
    if (name || social || tagline) return true
  }

  resetForm = () => {
    window.location.reload()
  }

  newRecord = () => {
    this.setState({ newRecord: !this.state.newRecord })
  }

  selectActor = ({ target: { value } }) => {
    const { name, social, tagline } = this.state.actors[value]
    const form = { name, social, tagline, id: value }
    this.setState({ form })
  }

  handleChange = ({ currentTarget: { id }, target: { value } }) => {
    const form = { ...this.state.form, [id]: value }
    this.setState({ form })
  }

  submit = e => {
    e.preventDefault()
    axios
      .post('/api/actors', this.state.form)
      .then(({ data: { error, message } }) => {
        this.setState({ error, message })
      })
  }

  delete = e => {}
  update = e => {}

  componentDidMount() {
    axios.get('/api/data').then(({ data: { data: { actors } } }) => {
      const ids = Object.keys(actors)
      this.setState({ actors, ids })
    })
  }

  render() {
    const thing = this.state.ids.map((id, i) => <option key={i}>{id}</option>)
    console.log('THING =>', this.unsaved())
    console.log(this.state)
    return (
      <div className="App">
        <div className="creator">
          <div className="messages">
            {this.state.error && (
              <div className="error">{this.state.error}</div>
            )}
            {this.state.message && (
              <div className="message">{this.state.message}</div>
            )}
          </div>
          <h1>Actor Management</h1>
          <Form className="creator-form">
            <Form.Label>
              {this.state.form.id || this.unsaved() ? (
                <Button onClick={this.resetForm} type="button">
                  Reset Form
                </Button>
              ) : (
                'Choose Actor'
              )}
            </Form.Label>
            {this.state.newRecord || this.unsaved() || (
              <Form.Control
                onChange={this.selectActor}
                defaultValue={-1}
                as="select"
              >
                <option disabled value={-1}>
                  - Choose Actor -
                </option>
                {this.state.ids.map((id, i) => (
                  <option key={i}>{id}</option>
                ))}
              </Form.Control>
            )}
            {this.state.fields.map((field, i) => {
              return (
                <Form.Group controlId={field} key={i}>
                  <Form.Label>{field}</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.form[field]}
                  ></Form.Control>
                </Form.Group>
              )
            })}
            {this.state.form.id ? (
              <>
                <Button onClick={this.delete}>Delete!</Button>
                <Button onClick={this.update}>Update!</Button>
              </>
            ) : (
              this.valid() && <Button onClick={this.submit}>Save!</Button>
            )}
          </Form>
        </div>
      </div>
    )
  }
}
