import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'

class Prompter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    axios.get('/api/state').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('state', data => {
      this.setState(data)
    })
  }

  render() {
    if (!this.state.view || !this.state.state)
      return <div className="App-header">LOADING...</div>

    if (!this.state.view.lines.current)
      return (
        <div className="App-header">
          SERVER ERROR! Tell Elijah he fucked up...
        </div>
      )

    const scriptLength = this.state.view.script.parsed.length
    const completed = this.state.state.line / (scriptLength - 1)
    const progressBarWidth = `${Math.floor(completed * 100)}%`
    const progressBarColor = `hsl(${completed * 120}deg, 100%, 50%)`
    const character = this.state.view.script.character
    const voice = this.state.view.script.voice
    const fontSize = 60 - 20 * (this.state.view.lines.current.length / 200)

    const chat = this.state.view.chat.map((item, i) => {
      const u = Math.abs(i / 5 - 1)
      return (
        <p
          key={item.id}
          className="prompter-chat"
          style={{
            fontSize: (u + 0.1) * 20 + 'pt',
            color: `#fff${Math.floor(u * 12).toString(16)}`,
          }}
        >
          {item.id}> {item.username}: {item.text}
        </p>
      )
    })

    const line = {
      previous: {
        text: this.state.view.lines.previous,
        size: fontSize / 2,
      },
      current: {
        text: this.state.view.lines.current,
        size: fontSize,
      },
      next: {
        text: this.state.view.lines.next,
        size: fontSize / 2,
      },
    }

    return (
      <div className="App">
        <header className="App-header">
          <div
            className="prompter-progress-bar"
            style={{
              width: progressBarWidth,
              backgroundColor: progressBarColor,
            }}
          ></div>
          {this.state.view && this.state.state && (
            <div className="part-info-header">
              <p className="part-info">{this.state.view.actor.name}</p>
              <p className="part-info">{this.state.view.project.name}</p>
              <p className="part-info">{this.state.view.script.name}</p>
              <p className="part-info">Take - {this.state.state.take}</p>
              <p className="part-info">
                {this.state.state.line} / {this.state.view.script.parsed.length}
              </p>
            </div>
          )}
          {line && (
            <>
              <p
                className="prompter-context-line"
                style={{ fontSize: line.previous.size }}
              >
                {line.previous.text}
              </p>
              <p
                className="prompter-current-line"
                style={{ fontSize: line.current.size }}
              >
                {line.current.text}
              </p>
              <p
                className="prompter-context-line"
                style={{ fontSize: line.next.size }}
              >
                {line.next.text}
              </p>
              <div className="prompter-footer">
                <div className="prompter-chat">{chat && chat}</div>
                <div className="prompter-context">
                  <p className="prompter-context-footer-line">
                    Character Type: {character}
                  </p>
                  <p className="prompter-context-footer-line">
                    Voice Type: {voice}
                  </p>
                </div>
              </div>
            </>
          )}
        </header>
      </div>
    )
  }
}

export default Prompter
