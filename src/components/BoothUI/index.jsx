import React from 'react'
import './index.css'
import axios from 'axios'
import redo from './redo.svg'
import io from 'socket.io-client'

class BoothUI extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      state: {},
      view: {},
      count: 0,
    }
  }

  componentDidMount() {
    axios.get('/api/state').then(({ data }) => {
      this.setState(data)
    })
    var socket = io()
    socket.on('state', ({ state, view }) => {
      this.setState({ state, view, count: this.state.count + 1 })
    })
  }

  handleRecordClick = e => {
    e.preventDefault()
    axios
      .post(`/api/state/recorder/${e.target.id}`)
      .then(({ data: { message, error } }) => {
        console.log(message, error)
        this.setState({ message, error, count: this.state.count + 1 })
      })
  }

  handleLineClick = e => {
    e.preventDefault()
    axios
      .post(`/api/state/line/${e.target.id}`)
      .then(({ data: { message, error } }) => {
        this.setState({ message, error, count: this.state.count + 1 })
      })
  }

  render() {
    console.log(this.state)
    const { recording } = this.state.state

    return (
      <div className="App">
        <header className="booth">
          <div className="booth-interface">
            <div
              id="trash"
              className="booth-interface-clicky"
              onClick={this.handleRecordClick}
            >
              Trash
            </div>
            <div
              className="booth-interface-clicky"
              id="soundboard"
              onClick={this.handleRecordClick}
            >
              Soundboard
            </div>
            <div
              className="booth-interface-clicky"
              id="restart"
              onClick={this.handleLineClick}
            >
              Restart
            </div>
          </div>
          <div className="booth-interface">
            <div
              id="previous"
              className="booth-button"
              onClick={this.handleLineClick}
            >
              &#60;
            </div>
            <img
              id="redo"
              src={redo}
              className="redo"
              alt="redo"
              onClick={this.handleLineClick}
            />
            <div
              id="next"
              className="booth-button"
              onClick={this.handleLineClick}
            >
              &#62;
            </div>
          </div>

          <div className="booth-interface">
            <div
              id="toggle_record"
              className={`booth-ui ${recording && 'recording'}`}
              onClick={this.handleRecordClick}
            >
              Toggle Recording
            </div>
            {this.state.error && (
              <div key={`${this.state.count}`} className="system-message error">
                {this.state.error}
              </div>
            )}
            {this.state.message && (
              <div
                key={`${this.state.count}`}
                className="system-message message"
              >
                {this.state.message}
              </div>
            )}
          </div>
        </header>
      </div>
    )
  }
}

export default BoothUI
