import React from 'react'
import './index.css'
import axios from 'axios'
import io from 'socket.io-client'
import { Form, Button } from 'react-bootstrap'

class ProjectCreator extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {},
    }
  }

  componentDidMount() {
    axios.get('/api/data').then(res => {
      this.setState(res.data)
    })
    var socket = io()
    socket.on('data', data => this.setState(data))
  }

  handleChange = e => {
    let { id, value } = e.currentTarget
    let { form } = this.state
    form[id] = value
    this.setState({ form })
  }

  handleSubmit = e => {
    e.preventDefault()
    let { form } = this.state

    axios.post('/api/projects', form).then(res => {
      let { error, message } = res.data
      this.setState({ error, message })
    })
  }

  render() {
    return (
      <div className="App">
        <header className="creator">
          <div className="messages">
            {this.state.error && (
              <div className="error">{this.state.error}</div>
            )}
            {this.state.message && (
              <div className="message">{this.state.message}</div>
            )}
          </div>
          <h1>Add Project</h1>
          <Form className="author-form" onSubmit={this.handleSubmit}>
            <Form.Group controlId="name">
              <Form.Label>Project Name</Form.Label>
              <Form.Control
                onChange={this.handleChange}
                type="text"
              ></Form.Control>
            </Form.Group>
            <Form.Text></Form.Text>
            <Form.Group controlId="author">
              <Form.Label>Project Author</Form.Label>
              <Form.Control
                onChange={this.handleChange}
                type="text"
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="website">
              <Form.Label>Project Website</Form.Label>
              <Form.Control
                onChange={this.handleChange}
                type="text"
              ></Form.Control>
              <Form.Text className="text-muted">
                website of the project for promos
              </Form.Text>
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </header>
      </div>
    )
  }
}

export default ProjectCreator
