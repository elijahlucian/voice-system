'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Scripts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      file: {
        type: Sequelize.STRING
      },
      character: {
        type: Sequelize.STRING
      },
      voice: {
        type: Sequelize.STRING
      },
      project_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      parsed: {
        type: Sequelize.JSONB
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Scripts');
  }
};