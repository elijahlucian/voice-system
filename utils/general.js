const random = require('random')

const rando = (array) => {
  return array[random.int(0,array.length - 1)]
}

const capitalize = (word) => {
  return word.charAt(0).toUpperCase() + word.slice(1)
}

const range = (limit) => {
  return [...Array(limit).keys()]
}

const snakeify = (str) => {
  return str.toLowerCase().replace(/\s/g, '_')
}

module.exports = { rando, capitalize, range, snakeify }