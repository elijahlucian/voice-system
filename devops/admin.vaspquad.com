
server {

    server_name admin.vapsquad.com;

    access_log /var/log/nginx/admin.vapsquad.com.access.log;
    error_log /var/log/nginx/admin.vapsquad.com.error.log;

    gzip on;
    gzip_types text/css application/javascript application/json image/svg+xml;
    gzip_comp_level 9;
    etag on;
    
    root /var/www/admin.vapsquad.com/html;

    location / {
	    try_files $uri $uri/ /index.html;
	    index index.html;
    }

    location ~ ^/api/ {
        proxy_pass http://localhost:8080;
  	    proxy_http_version 1.1;
	    proxy_set_header Upgrade $http_upgrade;
	    proxy_set_header Connection 'upgrade';
	    proxy_set_header Host $host;
	    proxy_cache_bypass $http_upgrade;
    }

    location ~ ^/socket.io/ {
	    proxy_pass http://localhost:8080;
    }

    error_page  405     =200 $uri;

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/admin.vapsquad.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/admin.vapsquad.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = admin.vapsquad.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    listen [::]:80;

    server_name admin.vapsquad.com;
    return 404; # managed by Certbot


}
