# copy latest nginx confif
scp root@104.248.218.151:/etc/nginx/sites-available/admin.vaspquad.com ./devops/

# copy all recorded files - run from vapsquad onedrive folder.
scp root@104.248.218.151:/root/voice-server/files/* ./files

ssh root@104.248.218.151 ls /root/voice-system/data/files


### build-deploy

git pull && react-scripts build && rsync -r ./build/* /var/www/admin.vapsquad.com/html
