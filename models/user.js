'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    social: DataTypes.STRING,
    tagline: DataTypes.STRING,
    email: DataTypes.STRING,
    type: DataTypes.INTEGER,
    id: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};