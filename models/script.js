'use strict';
module.exports = (sequelize, DataTypes) => {
  const Script = sequelize.define('Script', {
    name: DataTypes.STRING,
    file: DataTypes.STRING,
    character: DataTypes.STRING,
    voice: DataTypes.STRING,
    project_id: DataTypes.STRING,
    parsed: DataTypes.JSONB
  }, {});
  Script.associate = function(models) {
    Script.belongsTo(models.Project)
    // associations can be defined here
  };
  return Script;
};