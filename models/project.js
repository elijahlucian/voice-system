'use strict';
module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    name: DataTypes.STRING,
    user_id: DataTypes.STRING,
    website: DataTypes.STRING
  }, {});
  Project.associate = function(models) {
    Project.belongsTo(models.User)
    // associations can be defined here
  };
  return Project;
};